<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDataGamesDominoesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_games_dominoes', function (Blueprint $table) {
            $table->id();
            $table->foreignId('game_id')->index('game_id');
            $table->foreignId('domino_id')->index('domino_id');
            $table->smallInteger('player_number')->nullable();
            $table->timestamps();
        });

        Schema::table('data_games_dominoes', function (Blueprint $table) {
            $table->foreign('game_id')->references('id')->on('data_games')->onDelete('RESTRICT');
            $table->foreign('domino_id')->references('id')->on('definition_dominoes')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_games_dominoes');
    }
}
