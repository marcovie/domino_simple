<?php

use Illuminate\Database\Seeder;

class DefinitionDominoes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        DB::table('definition_dominoes')->truncate();

        //--------------------------------------------------------------------------------------------Tile side one is all zeros--------------------------------------------------------------------------------------------
        DB::table('definition_dominoes')->insert([
              'description' => '&#127075',//Blank
              'tile_side_one' => 0,
              'tile_side_two' => 0,
              'tile_double' => 1,//True - tile is a double tile - false tile is not a double tile
              'created_at' => now()->format('Y-m-d H:i:s'),
              'updated_at' => now()->format('Y-m-d H:i:s'),
          ]);

        DB::table('definition_dominoes')->insert([
            'description' => '&#127076',
            'tile_side_one' => 0,
            'tile_side_two' => 1,
            'tile_double' => 0,//True - tile is a double tile - false tile is not a double tile
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('definition_dominoes')->insert([
            'description' => '&#127077',
            'tile_side_one' => 0,
            'tile_side_two' => 2,
            'tile_double' => 0,//True - tile is a double tile - false tile is not a double tile
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('definition_dominoes')->insert([
            'description' => '&#127078',
            'tile_side_one' => 0,
            'tile_side_two' => 3,
            'tile_double' => 0,//True - tile is a double tile - false tile is not a double tile
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('definition_dominoes')->insert([
            'description' => '&#127079',
            'tile_side_one' => 0,
            'tile_side_two' => 4,
            'tile_double' => 0,//True - tile is a double tile - false tile is not a double tile
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('definition_dominoes')->insert([
            'description' => '&#127080',
            'tile_side_one' => 0,
            'tile_side_two' => 5,
            'tile_double' => 0,//True - tile is a double tile - false tile is not a double tile
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('definition_dominoes')->insert([
            'description' => '&#127081',
            'tile_side_one' => 0,
            'tile_side_two' => 6,
            'tile_double' => 0,//True - tile is a double tile - false tile is not a double tile
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);

        //--------------------------------------------------------------------------------------------Tile side one is all ones--------------------------------------------------------------------------------------------
        DB::table('definition_dominoes')->insert([
            'description' => '&#127083',
            'tile_side_one' => 1,
            'tile_side_two' => 1,
            'tile_double' => 1,//True - tile is a double tile - false tile is not a double tile
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('definition_dominoes')->insert([
            'description' => '&#127084',
            'tile_side_one' => 1,
            'tile_side_two' => 2,
            'tile_double' => 0,//True - tile is a double tile - false tile is not a double tile
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('definition_dominoes')->insert([
            'description' => '&#127085',
            'tile_side_one' => 1,
            'tile_side_two' => 3,
            'tile_double' => 0,//True - tile is a double tile - false tile is not a double tile
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('definition_dominoes')->insert([
            'description' => '&#127086',
            'tile_side_one' => 1,
            'tile_side_two' => 4,
            'tile_double' => 0,//True - tile is a double tile - false tile is not a double tile
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('definition_dominoes')->insert([
            'description' => '&#127087',
            'tile_side_one' => 1,
            'tile_side_two' => 5,
            'tile_double' => 0,//True - tile is a double tile - false tile is not a double tile
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('definition_dominoes')->insert([
            'description' => '&#127088',
            'tile_side_one' => 1,
            'tile_side_two' => 6,
            'tile_double' => 0,//True - tile is a double tile - false tile is not a double tile
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);

        //--------------------------------------------------------------------------------------------Tile side one is all twos--------------------------------------------------------------------------------------------
        DB::table('definition_dominoes')->insert([
            'description' => '&#127091',
            'tile_side_one' => 2,
            'tile_side_two' => 2,
            'tile_double' => 1,//True - tile is a double tile - false tile is not a double tile
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('definition_dominoes')->insert([
            'description' => '&#127092',
            'tile_side_one' => 2,
            'tile_side_two' => 3,
            'tile_double' => 0,//True - tile is a double tile - false tile is not a double tile
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('definition_dominoes')->insert([
            'description' => '&#127093',
            'tile_side_one' => 2,
            'tile_side_two' => 4,
            'tile_double' => 0,//True - tile is a double tile - false tile is not a double tile
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('definition_dominoes')->insert([
            'description' => '&#127094',
            'tile_side_one' => 2,
            'tile_side_two' => 5,
            'tile_double' => 0,//True - tile is a double tile - false tile is not a double tile
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('definition_dominoes')->insert([
            'description' => '&#127095',
            'tile_side_one' => 2,
            'tile_side_two' => 6,
            'tile_double' => 0,//True - tile is a double tile - false tile is not a double tile
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);

        //--------------------------------------------------------------------------------------------Tile side one is all threes--------------------------------------------------------------------------------------------
        DB::table('definition_dominoes')->insert([
            'description' => '&#127099',
            'tile_side_one' => 3,
            'tile_side_two' => 3,
            'tile_double' => 1,//True - tile is a double tile - false tile is not a double tile
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('definition_dominoes')->insert([
            'description' => '&#127100',
            'tile_side_one' => 3,
            'tile_side_two' => 4,
            'tile_double' => 0,//True - tile is a double tile - false tile is not a double tile
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('definition_dominoes')->insert([
            'description' => '&#127101',
            'tile_side_one' => 3,
            'tile_side_two' => 5,
            'tile_double' => 0,//True - tile is a double tile - false tile is not a double tile
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('definition_dominoes')->insert([
            'description' => '&#127102',
            'tile_side_one' => 3,
            'tile_side_two' => 6,
            'tile_double' => 0,//True - tile is a double tile - false tile is not a double tile
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);

        //--------------------------------------------------------------------------------------------Tile side one is all fours--------------------------------------------------------------------------------------------
        DB::table('definition_dominoes')->insert([
            'description' => '&#127107',
            'tile_side_one' => 4,
            'tile_side_two' => 4,
            'tile_double' => 1,//True - tile is a double tile - false tile is not a double tile
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('definition_dominoes')->insert([
            'description' => '&#127108',
            'tile_side_one' => 4,
            'tile_side_two' => 5,
            'tile_double' => 0,//True - tile is a double tile - false tile is not a double tile
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('definition_dominoes')->insert([
            'description' => '&#127109',
            'tile_side_one' => 4,
            'tile_side_two' => 6,
            'tile_double' => 0,//True - tile is a double tile - false tile is not a double tile
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);

        //--------------------------------------------------------------------------------------------Tile side one is all fives--------------------------------------------------------------------------------------------
        DB::table('definition_dominoes')->insert([
            'description' => '&#127115',
            'tile_side_one' => 5,
            'tile_side_two' => 5,
            'tile_double' => 1,//True - tile is a double tile - false tile is not a double tile
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('definition_dominoes')->insert([
            'description' => '&#127116',
            'tile_side_one' => 5,
            'tile_side_two' => 6,
            'tile_double' => 0,//True - tile is a double tile - false tile is not a double tile
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);

        //--------------------------------------------------------------------------------------------Tile side one is all sixes--------------------------------------------------------------------------------------------
        DB::table('definition_dominoes')->insert([
            'description' => '&#127123',
            'tile_side_one' => 6,
            'tile_side_two' => 6,
            'tile_double' => 1,//True - tile is a double tile - false tile is not a double tile
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);
        Schema::enableForeignKeyConstraints();
    }
}
