<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataGames extends Model
{
    const TABLE_NAME = 'data_games';

    protected $guarded = [];

    protected $table = self::TABLE_NAME;
}
