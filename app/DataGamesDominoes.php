<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataGamesDominoes extends Model
{
    const TABLE_NAME = 'data_games_dominoes';

    protected $guarded = [];

    protected $table = self::TABLE_NAME;
}
