<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DefinitionDominoes extends Model
{
    const TABLE_NAME = 'definition_dominoes';

    protected $guarded = [];

    protected $table = self::TABLE_NAME;
}
