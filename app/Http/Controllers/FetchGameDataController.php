<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\DataGames;
use App\DataGamesDominoes;
use App\DefinitionDominoes;

class FetchGameDataController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke($uuid)
    {
        try {
            $DataGames = DataGames::where('game_token', $uuid)->firstOrFail();

            $DataGamesDominoes = DataGamesDominoes::where('game_id', $DataGames->id)->get();
            $DefinitionDominoes_Result = DefinitionDominoes::get()->keyBy('id');

            if(!is_null($DataGamesDominoes) && count($DataGamesDominoes) > 0)
            {
                return response()->json([
                    'successful' => 6,
                    'message'    => 'Successfully loaded.',
                    'data' => $DataGamesDominoes,
                    'definitions' => $DefinitionDominoes_Result,
                    'functionName' => 'LoadAllTiles',
                    'players' => $DataGames->players
                ]);
            }

            return response()->json(array('successful' => 0, 'message' => 'An error occurred, please try again'));
        } catch (\Exception $e) {
            return response()->json(['successful' => 0, 'message' => 'An error occurred, please try again'.$e]);
        }
    }
}
