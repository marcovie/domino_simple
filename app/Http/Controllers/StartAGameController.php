<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

use App\DataGames;
use App\DataGamesDominoes;
use App\DefinitionDominoes;

class StartAGameController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $request->validate([
          'numberOfPlayers' => 'required|integer|min:2|max:4'
        ]);

        try {

            DB::beginTransaction();
            $DataGames                          = new DataGames;

            $uuid                               = (string) Str::uuid();
            $DataGames->game_token              = $uuid;
            $DataGames->players                 = $request->numberOfPlayers;
            $DataGamesSaved                     = $DataGames->save();

            $DefinitionDominoes_Result = DefinitionDominoes::all()->toArray();
            shuffle($DefinitionDominoes_Result);

            $DominoesCreationForGame_Result = true;
            for($x = 1;$x <= $request->numberOfPlayers;$x++)//player 1 is human player
            {
                $counter = 7;
                foreach($DefinitionDominoes_Result as $key => $internalArrayData)
                {
                    if($counter == 0)
                        break;

                    $DataGamesDominoesObj                     = new DataGamesDominoes;
                    $DataGamesDominoesObj->game_id            = $DataGames->id;
                    $DataGamesDominoesObj->domino_id          = $internalArrayData['id'];
                    $DataGamesDominoesObj->player_number      = $x;

                    if(!$DataGamesDominoesObj->save())
                        $DominoesCreationForGame_Result = false;
                    else
                    {
                        unset($DefinitionDominoes_Result[$key]);
                        $counter--;
                    }
                }
            }

            foreach($DefinitionDominoes_Result as $key => $internalArrayData)
            {
                $DataGamesDominoesObj                     = new DataGamesDominoes;
                $DataGamesDominoesObj->game_id            = $DataGames->id;
                $DataGamesDominoesObj->domino_id          = $internalArrayData['id'];

                if(!$DataGamesDominoesObj->save())
                    $DominoesCreationForGame_Result = false;
            }

            if($DominoesCreationForGame_Result && $DataGamesSaved) {
                DB::commit();
                return response()->json([
                    'successful' => 3,
                    'message'    => 'Successfully loaded.',
                    'data' => $uuid
                ]);
            }

            DB::rollBack();
             return response()->json(array('successful' => 0, 'message' => 'An error occurred, please try again'));
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['successful' => 0, 'message' => 'An error occurred, please try again'.$e]);
        }

    }
}
