<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('/start_game', 'StartAGameController')->name('start_game');
Route::get('/game/{id}', 'GameController')->name('game');
Route::get('/fetch-game-data/{id}', 'FetchGameDataController')->name('fetch_game_data');
