let axiosGetMixin = {
    methods: {
      axiosGetNormal(params, items, definitions) {
        axios.get(params.url, {
            params: {
              params,
            }
          }).then(({data}) => {
              if(!data.successful)//Fail -> 0 -> display message
              {
                // flash({"message":data.message, "type":"danger"});
              }
              else if(data.successful == 2)//success -> 2 -> display messag
              {
                // flash({"message":data.message, "type":"success"});
              }
              else if(data.successful == 3)//success -> 2 -> display messag
              {
                //No message needed.
              }
              else if(data.successful == 4)//success -> 3 -> display message -> populate data items and definitions
              {
                this.definitions = data.data.definitions;
                $.each(data.data, function(key, value) {
                    items.push(value);
                });
                // flash({"message":data.message, "type":"success"});
              }
              else if(data.successful == 5)//success -> 4 -> call function when completed -> populate data items and definitions
              {
                this.definitions = data.data.definitions;
                $.each(data.data, function(key, value) {
                    items.push(value);
                });
                this.onSuccess();
              }
              else if(data.successful == 6)
              {
                  this.definitions = data.definitions;
                  $.each(data.data, function(key, value) {
                      items.push(value);
                  });
                this[data.functionName](data.players);
              }
              else//success -> 0 -> display message -> populate data items
              {
                $.each(data.data, function(key, value) {
                    items.push(value);
                });
                // flash({"message":data.message, "type":"success"});
              }
          })
          .catch(error => {
              console.log(error);
            // flash({"message":error.response.data.message, "type":"danger"});
            if(error != 'Error: Request failed with status code 500')
              this.errors = error.response.data.errors;
           })
      },
    }
}

export default axiosGetMixin
